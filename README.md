
#Work Hour Input
This repository holds the source for the Salesforce-based work-hour input web application. This application is Node.js based but may be embedded into an IIS or Apache Tomcat instance. For prototyping purposes, this application only applies to weeks in 2015 and does not allow to query submitted hours.

###Some Notes
 - This application can be run independently on any machine with Node.js installed
 - To run the application, cd to the project's root directory and either run `npm start` or `node server`
 - The Node.js server is configured to run on port 3000 of the local machine

###Who to contact?
- Grant Peltier
- (972) 809 - 0275
- [grantpeltier@utexas.edu](mailto:grantpeltier@utexas.edu)
