/**
 * Created by Grant Peltier on 9/30/15.
 */
var express = require("express");
var nforce = require("nforce");
var bodyParser = require("body-parser");
var app = express();

//Middleware
app.use(bodyParser.json());

var clientID = "3MVG9KI2HHAq33Rxyp9bLMVijJbyvVuq4GQ4Jn2q_McAu5D2ltVXngn.TDLtK1LwWeSTb0FMrjQauqZlk3u82";
var clientSecret = "435964561811118835";

var org = nforce.createConnection({
    clientId: clientID,
    clientSecret: '435964561811118835',
    redirectUri: 'http://localhost:3000/oauth/_callback',
    apiVersion: 'v27.0',  // optional, defaults to current salesforce API version
    environment: 'production',  // optional, salesforce 'sandbox' or 'production', production default
    mode: 'multi' // optional, 'single' or 'multi' user mode, multi default
});


var oauth;
org.authenticate({ username: 'grantpeltier@utexas.edu', password: 'frosty12'}, function(err, resp){
    // store the oauth object for this user
    if(!err) oauth = resp;
    console.log(resp);
});

/* serves main page */
app.get("/", function(req, res) {
    res.sendfile('index.html')
});

app.post("/user/add", function(req, res) {
    /* some server side logic */
    res.send("OK");
});

app.post("/api/sendTimes", function(req, res){
    var acc = nforce.createSObject('Time__c');
    console.log("Start Day: "+ req.body.startDay);
    console.log("Sunday time is: " + req.body.sunday);
    acc.set('Sunday__c', req.body.sunday);
    acc.set('Monday__c', req.body.monday);
    acc.set('Tuesday__c', req.body.tuesday);
    acc.set('Wednesday__c', req.body.wednesday);
    acc.set('Thursday__c', req.body.thursday);
    acc.set('Friday__c', req.body.friday);
    acc.set('Saturday__c', req.body.saturday);
    acc.set('WeekStartDay__c', req.body.startDay);

    org.insert({ sobject: acc, oauth: oauth }, function(err, resp){
        if(!err){
            console.log('Insert worked!');
            res.sendStatus(200);
            //resp.status(200).send('OK');
        }
        else{
            console.log("An error occurred while inserting: " + err);
            res.sendStatus(500);
            //resp.status(500).send('FAIL');
        }
    });
});

/* serves all the static files */
app.get(/^(.+)$/, function(req, res){
    console.log('static file request : ' + req.params);
    res.sendfile( __dirname + req.params[0]);
});

var port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log("Listening on " + port);
});
