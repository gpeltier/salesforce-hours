// Debugging options
var debug = true;

function log(str){
    if(debug){console.log(str);}
}


var app = angular.module('app',[]);


//pad 1-digit numbers with a leading zero
function pad2(num){
    var str = num + "";
    while(str.length < 2){str = "0" + str;}
    return str;
}


//Get first week of month. ISO 8601 Does not use zero-indexing
function getStartWeek(month){
    var firstDay = moment("2015-"+pad2(month+1)+"-01");
    return firstDay.week();

}


function DateTimeObject(data){
    var self = this;
    self.weekday = data.weekday;
    self.month = data.month;
    self.date = data.day;
    self.hoursWorked = 0;
}


// Main business logic for time input application
app.controller('DateController', function($scope, timeService){

    $scope.selectedMonth = {};
    $scope.months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $scope.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    $scope.sundays = [];
    $scope.inputDays = [];
    $scope.selectedWeekStart = {};



    $scope.getWeeklyHours = function(){
        var total = 0;
        for(var i = 0; i < $scope.inputDays.length; i++){
            total += $scope.inputDays[i].hoursWorked;
        }
        return total;
    };

    $scope.updateDateTimes = function(){
        log(day);
    };

    $scope.showWeeks = function(monthIndex){
        $scope.selectedMonth = {"index": monthIndex, "text":$scope.months[monthIndex]};
        log("Month Index: " + monthIndex);
        var startWeek = getStartWeek(monthIndex);
        log("Start Week is: " + startWeek);
        var startDay = moment("2015-W"+pad2(startWeek)+"-7");
        if(startDay.date()-7 == 1){startDay.day(-7);}               //moment.js is expceptional when Sunday is first of month
        //var startDay = moment("2015-"+pad2(monthIndex+1)+"-01");
        startDay.day(0);
        log("Moment month= " + startDay.month());
        log("Moment week= " + startDay.week());
        log("Moment day= " + startDay.date());
        $scope.sundays = [];
        while(startDay.month() <= monthIndex && startDay.year() == 2015){
            var text = "Week of the " + startDay.date();
            if(startDay.date() == 1 || startDay.date() == 21 || startDay.date() == 31){     //annoying number suffixes
                text += "st";
            }else if(startDay.date() == 2 || startDay.date() == 22){
                text += "nd";
            }else if(startDay.date() == 3 || startDay.date() == 23){
                text += "rd";
            }else{text += "th";}
            $scope.sundays.push({"month": startDay.month(),"day": startDay.date(), "text": text });
            log("Day of Month: " + startDay.date());
            startDay.add(7, 'days');
        }
    };

    $scope.displayTimeForm = function(day, month){
        log("week clicked starts: " + day + "/" + month);
        $scope.inputDays = [];
        var currentDay = moment([2015, month, day]);
        $scope.selectedWeekStart = currentDay.toISOString();
        for(var i =0; i < 7; i++){
            $scope.inputDays.push(new DateTimeObject({"weekday": $scope.days[currentDay.weekday()],"month": currentDay.month(),"day": currentDay.date()}));
            currentDay.add(1, 'd');
        }
    };

    validateInput = function(){
        if($scope.inputDays.length == 0) return false;
        for(var i =0; i < $scope.inputDays.length; i++){
            var inVal = document.getElementById("num_"+$scope.inputDays[i].weekday).value;
            var val = $scope.inputDays[i].hoursWorked;
            if(/\D/.test(inVal) || inVal < 0 || inVal > 24 || !inVal){
                return false;
            }
        }
        return true;
    }


    $scope.submitTimes = function(){
        log("Submitting times...");
        //$scope.$digest();
        if(!validateInput()){
            swal("Uh oh...", "Please only enter numbers between 0 and 24 for each day", "error");
            return;
        }


        var weekTimeObj = {
            sunday: $scope.inputDays[0].hoursWorked,
            monday: $scope.inputDays[1].hoursWorked,
            tuesday: $scope.inputDays[2].hoursWorked,
            wednesday: $scope.inputDays[3].hoursWorked,
            thursday: $scope.inputDays[4].hoursWorked,
            friday: $scope.inputDays[5].hoursWorked,
            saturday: $scope.inputDays[6].hoursWorked,
            startDay: $scope.selectedWeekStart
        };
        timeService.addTimes(weekTimeObj);
    }


});

app.service("timeService", function($http, $q){
    // Return public API
    return({addTimes: addTimes});


    function addTimes(times){
        var jsonTimes = JSON.stringify(times);
        log("JSON Data: "+jsonTimes);
        swal({
            title: "Submit Times?",
            text: "Are you sure your times are correct?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $http({
                method: 'POST',
                url: 'api/sendTimes',
                data: jsonTimes,
                headers: {'Content-Type': 'application/json'}
            }).then(function success(resp){
                log(resp);
                swal("Submitted!", "Your hours have been submitted", "success");
            }, function err(resp){
                log(resp);
                swal("Oops...", "Something went wrong!", "error");
            });
        });

    }
});
